class ArticlesController < ApplicationController
	
	#Аутентификация на создание, изменение
	http_basic_authenticate_with name: "admin", password: "admin", except: [:index, :show]
	
	#Главная
	def index
		@articles = Article.all
	end
	
	#Показать
	def show
		@article = Article.find(params[:id])
	end
	
	#Новая
	def new
		@article = Article.new
	end
	
	#Изменить
	def edit		
		@article = Article.find(params[:id])
	end
	
	#Создать
	def create
		@article = Article.new(article_params)
		if @article.save
			redirect_to @article
		else
			render 'new'
		end
	end
	
	#Изменить
	def update		
		@article = Article.find(params[:id])
		if @article.update(article_params)
			redirect_to @article
		else
			render 'edit'
		end
	end
	
	#Удалить
	def destroy		
		@article = Article.find(params[:id])
		@article.destroy
		redirect_to articles_path
	end
	
	#Объяление
	private
		def article_params
			params.require(:article).permit(:title, :text)
		end
	
end
